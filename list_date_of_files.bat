@ECHO OFF
SETLOCAL ENABLEDELAYEDEXPANSION
SET "sourcedir=."

del raw_files.txt >nul 2>&1

FOR /f "delims=" %%a IN (
 'dir /b /s /ad "%sourcedir%\*.raw" '
 ) DO (
 CALL :creationdate "%%a" c crdatetime
 CALL :creationdate "%%a" w wrdatetime
 CALL :creationdate "%%a" a acdatetime
 rem ECHO !crdatetime!;!wrdatetime!;!acdatetime!;%%a
 ECHO %%~da%%~pa;%%~na%%~xa;!crdatetime! >> raw_files.txt

)

GOTO :EOF

:creationDate
for /f "skip=5 tokens=1,2,3 delims= " %%a in (
 'dir  /ad /o:d /t:%2 "%~1"') do set "%~3=%%~a;%%~b %%~c"&goto:eof
goto:eof
