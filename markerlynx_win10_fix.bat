@ECHO OFF

echo Now we try to fix the windows 10 errors you got at installation
echo You should be prompted several times about files being registered
echo Just click OK
cd c:\MassLynx
Regsvr32.exe MLynxInstrumentProxy.dll
cd PDACalib
Regsvr32.exe OptInt.dll
cd ..
Regsvr32.exe MLynxSLE.ocx
Regsvr32.exe MLynxQueueProxy.dll
echo We should be done now.
pause
