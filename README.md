# Snippets

## Fixing crashing MarkerLynx on windows 10
[markerlynx_win10_fix.bat](markerlynx_win10_fix.bat)

## convert masshunter .d files
[convert_masshunter_GC.bat](ms data conversion/convert_masshunter_GC.bat)

Data from chemstation (older instruments) first need to be converted to a newer .d format using "GC MSD translator"/"MassHunter GC/MS Translator" which is on a masshunter supplemental disk.
With my data I then needed to use --simAsSpectra to get normal spectral data (it is saved as if it was SIM for each nominal mass).

## Take a text file with offending emails and count users from same domain
[spam_domains.R](spam_domains.R)

## msconvert in parallel skipping existing files
[msconvert_par.R](ms data conversion/msconvert_par.R)
